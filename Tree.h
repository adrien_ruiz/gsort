#ifndef _TREE_H_
#define _TREE_H_

#include "Node.h"

class Tree {
    private:
        Node* createNode(int value);
        void addNodeRecursive(Node* root, Node* node);
        
    public:
        Node *root;
        
        Tree(int value);
        virtual ~Tree();
        
        Node* addValue(int value);
};

#endif